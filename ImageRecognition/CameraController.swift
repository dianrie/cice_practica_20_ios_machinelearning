//
//  AppDelegate.swift
//  MachineLearning
//
//  Created by Diego Angel on 05/5/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: UIViewController {

    @IBOutlet weak var temporizadorLabel: UILabel!
    @IBOutlet weak var buscarLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var buscar = ["mouse, computer mouse","monitor","modem","computer keyboard, keypad","iPod","joystick", "desk","digital watch","sandal"]
    
    var ramdom = 0
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var puntuacion = 0
    var count = 20
    var timer = Timer()
    
    var mlModel = Inceptionv3()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ramdom = Int.random(in: 0..<buscar.count)
        buscarLabel.text = "Busca: " + buscar[ramdom]
     
        configure()
        startCounter()
    }
    func startCounter() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CameraController.counter), userInfo: nil, repeats: true)
    }
    
    @objc func counter() {
        
        count -= 1
        
        temporizadorLabel.text = "\(count)"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Start video capture.
        captureSession.startRunning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        captureSession.stopRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func configure() {
        // Get the back-facing camera for capturing videos
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
            print("Failed to get the camera device")
            return
        }
 
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            //Creamos el Data Output para nuestro modelo
            let videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "imageRecognition.queue"))
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            captureSession.addOutput(videoDataOutput)
                        
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Bring the label to the front
        descriptionLabel.text = "Looking for objects..."
        view.bringSubview(toFront: descriptionLabel)
        view.bringSubview(toFront: buscarLabel)
        view.bringSubview(toFront: temporizadorLabel)
    }
    
    //Funcion generica para mostra alertas
    func alerta( titulo: String, contenido: String){
        let alertController = UIAlertController(title: titulo, message:
            contenido, preferredStyle: .alert)
        //alertController.addAction(UIAlertAction(title: "OK", style: .default))
        alertController.addAction(UIAlertAction(title: "OTRA VEZ", style: .default, handler: { (UIAlertAction) in
            self.ramdom = Int.random(in: 0..<self.buscar.count)
            self.buscarLabel.text = "Busca: " + self.buscar[self.ramdom]
            self.count = 20
            self.startCounter()
        }))
       // self.present(alertController, animated: true, completion: nil)
        self.present(alertController, animated: true, completion: {
            self.timer.invalidate()
            self.puntuacion = 0
            self.temporizadorLabel.text = "00"
            self.count = 2
            
        } )
    }
}

extension CameraController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if self.count < 1 {
            self.alerta(titulo: "Se acabo el tiemo ", contenido: "No pudiste encotrar un: " + buscar[ramdom] + "\n Puntuación: \(puntuacion)")
        }else{
        
            connection.videoOrientation = .portrait
            //Re-escalamos la imagen a 299x299 (lo que nos pide el modelo)
            guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
                return
            }
        
            //Creamos la imagen con la info del buffer
            let imagenCore = CIImage(cvImageBuffer: imageBuffer)
            let imagen = UIImage(ciImage: imagenCore)
            UIGraphicsBeginImageContext(CGSize(width: 299, height: 299))
            imagen.draw(in: CGRect(x: 0, y: 0, width: 299, height: 299))
            let imagenReescalada = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        
            //Pasamos de CIImage a CVPixelBuffer
            let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        
            var pixelBuffer: CVPixelBuffer?
        
            let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(imagenReescalada.size.width), Int(imagenReescalada.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        
            guard status == kCVReturnSuccess else {
                return
            }
        
            CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
            // ---------------- ---------- ----------
            let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
            let context = CGContext(data: pixelData,
                                    width: Int(imagenReescalada.size.width),
                                    height: Int(imagenReescalada.size.height),
                                    bitsPerComponent: 8,
                                    bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!),
                                    space: rgbColorSpace,
                                    bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        
            context?.translateBy(x: 0, y: imagenReescalada.size.height)
            context?.scaleBy(x: 1.0, y: -1.0)
            UIGraphicsPushContext(context!)
        
            imagenReescalada.draw(in: CGRect(x: 0, y: 0, width: imagenReescalada.size.width, height: imagenReescalada.size.height))
            UIGraphicsPopContext()
            // ---------------- ---------- ----------
            CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        
            //Le pasamos la imagen al modelo y nos devuelve la clase de la imagen
            if let pixelBuffer = pixelBuffer {
                if let output = try? mlModel.prediction(image: pixelBuffer) {
                    DispatchQueue.main.async {
                        if self.count < 5 {
                            self.temporizadorLabel.backgroundColor = #colorLiteral(red: 0.9330877591, green: 0.4498164318, blue: 0.4320568965, alpha: 0.48064157)
                        }else{
                            if self.count < 10 {
                                self.temporizadorLabel.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6274290727, blue: 0.331159489, alpha: 0.5)
                            }else{
                            self.temporizadorLabel.backgroundColor = #colorLiteral(red: 0.7529063893, green: 0.9330877591, blue: 0.7501392851, alpha: 0.8470588235)
                            }
                        }
                        if self.buscar[self.ramdom] == output.classLabel {
                            self.descriptionLabel.backgroundColor = #colorLiteral(red: 0.7529063893, green: 0.9330877591, blue: 0.7501392851, alpha: 0.5)
                            self.descriptionLabel.text = output.classLabel
                            self.ramdom = Int.random(in: 0..<self.buscar.count)
                            self.buscarLabel.text = "Busca: " + self.buscar[self.ramdom]
                            self.count = 20
                            self.puntuacion += 1
                        
                        }else{
                            self.descriptionLabel.backgroundColor = #colorLiteral(red: 0.9330877591, green: 0.4498164318, blue: 0.4320568965, alpha: 0.48064157)
                            self.descriptionLabel.text = output.classLabel
                            self.buscarLabel.text = "Busca: " + self.buscar[self.ramdom]
                            //print(output.classLabel)
                        }
                    
                    
                        // for (clave, valor) in output.classLabelProbs {
                        //     print("\(clave): \(valor)")
                        // }
                    }
                }
            }
        }
    }
}


